package user.TestNG.testcases;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DemoTestNG {

	public String baseUrl="https://gmail.com";
	public WebDriver driver;
	
	@BeforeTest
	public void setBaseURL()
	{
		System.setProperty("webdriver.chrome.driver", "C:/Users/Raghav/Desktop/TestNG_Gmail/driver/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get(baseUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		// declare and initialize the variable to store the expected title of the webpage.
					String expectedTitle = "Gmail";	              

					/*
					 * The getTitle() method is used to fetch the title of the current web page. 
					 * Thus, the fetched title can be loaded to a string variable.
					 */
					String actualTitle = driver.getTitle();
					
					Assert.assertEquals(expectedTitle,actualTitle);
					
	
	}
	
	
	
	
	@Test
	public void verifyLogin()
	{
		driver.findElement(By.id("Email")).sendKeys("Enter your Email here");			
		driver.findElement(By.id("next")).click();
					
	}
	
	@AfterTest
	public void endSession()
	{
		driver.findElement(By.id("Passwd")).sendKeys("Enter the pwd here");		
		driver.findElement(By.id("signIn")).click();	
		// close the web browser
					driver.close();			
	}
}