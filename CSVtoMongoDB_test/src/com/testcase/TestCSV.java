package com.testcase;

import java.net.UnknownHostException;

import org.testng.annotations.Test;

import com.dataprovider.DataProviderClass;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class TestCSV {	
	@Test(dataProvider="getDataFromCSV",dataProviderClass=DataProviderClass.class)
	//street,city,zip,state,beds,baths,sq__ft,type,sale_date,price,latitude,longitude
	public void testCaseCsv(String street,String City,String Zip,String State,String beds,
			String baths, String sqfeet,String type,String sale_date,String price, String latitute, String logitude)throws UnknownHostException{
		
		System.out.println("Street :"+street);
		System.out.println("City :"+City);
		System.out.println("Zip :"+Zip);
		System.out.println("State :"+State);	
		DB dB=(new MongoClient("localhost",27017)).getDB("TestDB");//database name
		DBCollection dBCollection=dB.getCollection("Sample2");//table name
		BasicDBObject basicDBObject=new BasicDBObject();
		basicDBObject.put("Street",street);//field1
		basicDBObject.put("City",City);//field2
		basicDBObject.put("Zip",Zip);//field3
		basicDBObject.put("State",State);//field4
		
		basicDBObject.put("Beds",beds);//field5		
		basicDBObject.put("Baths",baths);//field6
		basicDBObject.put("Square_Feet",sqfeet);//field7
		basicDBObject.put("Type",type);//field8
		
		basicDBObject.put("Sale_Date",sale_date);//field9		
		basicDBObject.put("Price",price);//field10
		basicDBObject.put("Latitude",latitute);//field11
		basicDBObject.put("Longitude",logitude);//field12
		
		dBCollection.insert(basicDBObject);
		
	}
}

