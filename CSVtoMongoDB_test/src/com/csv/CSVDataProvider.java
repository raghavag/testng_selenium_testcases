package com.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.csvreader.CsvReader;

public class CSVDataProvider {
	private static CsvReader reader=null;
	private static Object[][] data=null;	
	private static int getRow(String fileName)
	{ 		
		File file=new File(fileName);
		Scanner s=null;
		String InputLine="";
		int Rows=0;
		int Columns=0;
		try
		{
			s=new Scanner(file);			
			while(s.hasNext())
			{				
				InputLine=s.nextLine();
				String[] values=InputLine.split(",");
				Rows++;
				Columns=values.length;
				
			}
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}	
		return Rows;
		
	}
	private static int getColumn(String fileName)
	{ 	
		File file=new File(fileName);
		Scanner s=null;
		String InputLine="";
		int Rows=0;
		int Columns=0;
		try
		{
			s=new Scanner(file);			
			while(s.hasNext())
			{				
				InputLine=s.nextLine();
				String[] values=InputLine.split(",");
				Rows++;
				Columns=values.length;
				
			}
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}	
		return Columns;
	}
	private static void getData(String fileName)
	{
		int i=0;
		int columns=getColumn(fileName);		
		int rows=getRow(fileName);
		
		//int flag=columns;
		
		System.out.println("Rows getDATA:"+rows);
		System.out.println("Columns getData:"+columns);
		try
		{
			data=new Object[rows][columns];
			reader=new CsvReader(fileName);
			while(reader.readRecord()){
				data[i][0]=reader.get(0);
				data[i][1]=reader.get(1);
				data[i][2]=reader.get(2);
				data[i][3]=reader.get(3);
				data[i][4]=reader.get(4);
				data[i][5]=reader.get(5);
				data[i][6]=reader.get(6);
				data[i][7]=reader.get(7);
				data[i][8]=reader.get(8);
				data[i][9]=reader.get(9);
				data[i][10]=reader.get(10);
				data[i][11]=reader.get(11);
				
				i++;
			}			
		}
		catch (Exception e) {
			// TODO: handle exception
		}		
	}
	public static Object[][] getCSVData(String fileName){
		getData(fileName);
		return data;
	}
}

